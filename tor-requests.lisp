;;;; tor-requests.lisp

(in-package #:tor-requests)

(defvar *webdriver*)

(defun get-url-selenium (url &optional (tor-socks-proxy "socks5://127.0.0.1:9050"))
  (let* ((get-method (java:jmethod "org.openqa.selenium.chrome.ChromeDriver" "get" "java.lang.String"))
         (chrome-options (java:jnew "org.openqa.selenium.chrome.ChromeOptions"))
         (add-argument-method (java:jmethod "org.openqa.selenium.chrome.ChromeOptions"
                                            "addArguments" "java.lang.String[]"))
         (proxy-arg (java:jarray-from-list (list (format nil "--proxy-server=~a" tor-socks-proxy)))))
    (java:jcall add-argument-method chrome-options proxy-arg)
    (setf *webdriver* (java:jnew "org.openqa.selenium.chrome.ChromeDriver" chrome-options))
    (java:jcall get-method *webdriver* url)))
