# tor-requests
### _Alejandro Zamora Fonseca_

A client to make web requests under Tor with the help of Selenium

Use:

curl -x socks5h://localhost:9050 -s https://check.torproject.org/api/ip

in order to check if we are connected to a Tor proxy before running the library

Also check you have the chrome-webdriver executable in your PATH

After that you can use this library followingt these steps:

```
CL-USER> (require :asdf)
(require :abcl-contrib)
(require :quicklisp-abcl)
(require :abcl-asdf)

NIL
CL-USER> (ql:quickload :tor-requests)
To load "tor-requests":
  Load 1 ASDF system:
    tor-requests
; Loading "tor-requests"

(:TOR-REQUESTS)
CL-USER> (tor-requests::get-url-selenium "https://gnu.org")
NIL
CL-USER>
```

You should a chrome browser opened to the link at GNU page

You can also check that you are using TOR with https://www.expressvpn.com/what-is-my-ip, compare with the "normal" browser" wich points your location.
