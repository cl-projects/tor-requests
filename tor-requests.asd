;;;; tor-requests.asd

(asdf:defsystem #:tor-requests
  :description "Make web requests using the Tor network"
  :author "Alejandro Zamora Fonseca"
  :version "0.0.1"
  :serial t
  :components ((:mvn "org.seleniumhq.selenium/selenium-java/3.141.59")
               (:mvn "org.jsoup/jsoup/1.16.1")
               (:file "package")
               (:file "tor-requests")))
